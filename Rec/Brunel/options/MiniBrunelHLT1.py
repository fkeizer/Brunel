from Configurables import MiniBrunel

mbrunel = MiniBrunel()
mbrunel.EvtMax = 100
mbrunel.ThreadPoolSize = 2
mbrunel.EventSlots = 2
mbrunel.CallgrindProfile = True
mbrunel.HLT1Only = True
mbrunel.EnableHive = True
