from Gaudi.Configuration import *

importOptions('$APPCONFIGOPTS/Brunel/MC-WithTruth.py')
importOptions('$APPCONFIGOPTS/Brunel/DataType-2012.py')
importOptions('$APPCONFIGOPTS/Brunel/patchUpgrade1.py')

from Configurables import GaudiSequencer
seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += [ "GenFSRMerge" ]
seqGenFSR.Members += [ "GenFSRLog" ]

ApplicationMgr().TopAlg += [seqGenFSR]

from Configurables import LHCbApp, Brunel, TrackToDST
LHCbApp().DDDBtag   = "dddb-20130929-1"
LHCbApp().CondDBtag = "sim-20130522-1-vc-md100"
TrackToDST("FilterMuonTrackStates").TracksInContainer = "/Event/Rec/Track/Muon"

from PRConfig import TestFileDB
TestFileDB.test_file_db["genFSR_2012_digi"].run(configurable=Brunel())
